<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Post;
use App\Models\Outlet;
use App\User;


use Illuminate\Http\Request;
use Carbon\Carbon;

class PostsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;


        $from = Carbon::now('Asia/Jakarta')->subDays(1)->format('Y-m-d');
        $to = Carbon::now('Asia/Jakarta')->format('Y-m-d');


        if($request->has('from') && $request->from != ""){
            $from = $request->from;
        }

        if ($request->has('to') && $request->to != "") {
            $to = $request->to;
        }


        $logged = auth()->user();
        $sql = null;
        if (!empty($keyword)) {
            if("Staff" == $logged->role){
                $between = [$from . " 00:00:00", $to . " 23:59:00"];
                $sql = Post::where('outlet_id', $logged->outlet_id)
                ->whereBetween('reservasi_date',$between)
                ->where('status','RESERVED')
                ->where(function ($query) use ($keyword){
                    $query->where('name', 'LIKE', "%$keyword%")
                        ->orWhere('phone', 'LIKE', "%$keyword%")
                        ->orWhere('pax', 'LIKE', "%$keyword%")
                        ->orWhere('outlet_id', 'LIKE', "%$keyword%")
                        ->orWhere('area', 'LIKE', "%$keyword%")
                        ->orWhere('segmentasi', 'LIKE', "%$keyword%")
                        ->orWhere('description', 'LIKE', "%$keyword%")
                        ->orWhere('reservasi_date', 'LIKE', "%$keyword%");
                });

            }else{
                $between = [$from . " 00:00:00", $to . " 23:59:00"];
                $sql = Post::whereBetween('reservasi_date',$between)
                ->where('status','RESERVED')
                ->where(function ($query) use ($keyword){
                    $query->where('name', 'LIKE', "%$keyword%")
                        ->orWhere('phone', 'LIKE', "%$keyword%")
                        ->orWhere('pax', 'LIKE', "%$keyword%")
                        ->orWhere('outlet_id', 'LIKE', "%$keyword%")
                        ->orWhere('area', 'LIKE', "%$keyword%")
                        ->orWhere('description', 'LIKE', "%$keyword%")
                        ->orWhere('segmentasi', 'LIKE', "%$keyword%")
                        ->orWhere('reservasi_date', 'LIKE', "%$keyword%");
                });
            }
            
        } else {
            $between = [$from . " 00:00:00", $to . " 23:59:00"];
            if("Staff" == $logged->role){
                $sql = Post::where('outlet_id', $logged->outlet_id)->where('status','RESERVED')->whereBetween('reservasi_date',$between);
            }else{
                $sql = Post::whereBetween('reservasi_date',$between)->where('status','RESERVED');
            }
        }

        $posts = $sql->latest()->paginate($perPage);
        $total = Post::whereBetween('reservasi_date',[$from, $to])->count();
        $dine_in = Post::whereBetween('reservasi_date',[$from, $to])->where('status','DINE IN')->count();
        $reserved =  Post::whereBetween('reservasi_date',[$from, $to])->where('status','RESERVED')->count();

        $user = User::count();
        $outlet = Outlet::count();


        return view('posts.index', compact('posts','from','to','keyword','total','dine_in','reserved','user','outlet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $outlets = Outlet::all();
        $logged = Auth()->user();

        if($logged->role != "Administrator"){
            $outlets = Outlet::where('id', $logged->outlet_id)->get();
        }

        return view('posts.create', compact('outlets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $post = new Post;
        $post->created_by = auth()->user()->name;
        $post->updated_by = auth()->user()->name;
        $post->name = $request->name;
        $post->segmentasi = $request->segmentasi;
        $post->phone = $request->phone;
        $post->pax = $request->pax;
        $post->outlet_id = $request->outlet_id;
        $post->area = $request->area;
        $post->description = $request->description;
        $post->reservasi_date = $request->reservasi_date;
        $post->save();


        return redirect('posts')->with('flash_message', 'Post added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);

        $outlets = Outlet::all();
        $logged = Auth()->user();

        if ($logged->role != "Administrator") {
            $outlets = Outlet::where('id', $logged->outlet_id)->get();
        }


        return view('posts.edit', compact('post','outlets'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function cancel($id)
    {
        $post = Post::find($id);
        $post->status = "CANCELED";
        $post->save();

        return redirect('posts')->with('flash_message', 'CANCELED');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
    
        $post = Post::findOrFail($id);
        $post->updated_by = auth()->user()->name;
        $post->name = $request->name;
        $post->phone = $request->phone;
        $post->segmentasi = $request->segmentasi;

        $post->pax = $request->pax;
        $post->outlet_id = $request->outlet_id;
        $post->area = $request->area;
        $post->description = $request->description;
        $post->reservasi_date = $request->reservasi_date;
        $post->save();

        $post->save();

        return redirect('posts')->with('flash_message', 'Post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->status = "DINE IN";
        //$post->deleted_at = Carbon::now('Asia/Jakarta');
        $post->save();

        return redirect('posts')->with('flash_message', 'DINE IN!');
    }
}
