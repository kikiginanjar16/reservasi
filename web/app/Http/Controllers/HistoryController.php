<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Post;
use App\Models\Outlet;
use App\User;


use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class HistoryController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $posts = Post::distinct('phone')->paginate(10);
        return view('history.index', compact('posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function detail($phone)
    {
        $posts = Post::where('phone', $phone)->get();
        return view('history.detail', compact('posts'));
    }
}
