<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Antrian;
use App\Models\Post;
use App\Models\Outlet;
use App\User;


use Illuminate\Http\Request;
use Carbon\Carbon;

class AntrianController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;


        $from = Carbon::now('Asia/Jakarta')->subDays(1)->format('Y-m-d');
        $to = Carbon::now('Asia/Jakarta')->format('Y-m-d');


        if($request->has('from') && $request->from != ""){
            $from = $request->from;
        }

        if ($request->has('to') && $request->to != "") {
            $to = $request->to;
        }


        $logged = auth()->user();
        $sql = null;
        if (!empty($keyword)) {
            if("Staff" == $logged->role){
                $between = [$from . " 00:00:00", $to . " 23:59:00"];
                $sql = Antrian::where('outlet_id', $logged->outlet_id)
                ->whereBetween('reservasi_date',$between)
                ->where('status','QUEUE')
                ->where(function ($query) use ($keyword){
                    $query->where('name', 'LIKE', "%$keyword%")
                        ->orWhere('phone', 'LIKE', "%$keyword%")
                        ->orWhere('pax', 'LIKE', "%$keyword%")
                        ->orWhere('outlet_id', 'LIKE', "%$keyword%")
                        ->orWhere('area', 'LIKE', "%$keyword%")
                        ->orWhere('segmentasi', 'LIKE', "%$keyword%")
                        ->orWhere('description', 'LIKE', "%$keyword%")
                        ->orWhere('reservasi_date', 'LIKE', "%$keyword%");
                });

            }else{
                $between = [$from . " 00:00:00", $to . " 23:59:00"];
                $sql = Antrian::whereBetween('reservasi_date',$between)
                ->where('status','QUEUE')
                ->where(function ($query) use ($keyword){
                    $query->where('name', 'LIKE', "%$keyword%")
                        ->orWhere('phone', 'LIKE', "%$keyword%")
                        ->orWhere('pax', 'LIKE', "%$keyword%")
                        ->orWhere('outlet_id', 'LIKE', "%$keyword%")
                        ->orWhere('area', 'LIKE', "%$keyword%")
                        ->orWhere('description', 'LIKE', "%$keyword%")
                        ->orWhere('segmentasi', 'LIKE', "%$keyword%")
                        ->orWhere('reservasi_date', 'LIKE', "%$keyword%");
                });
            }
            
        } else {
            $between = [$from . " 00:00:00", $to . " 23:59:00"];
            if("Staff" == $logged->role){
                $sql = Antrian::where('outlet_id', $logged->outlet_id)->where('status','QUEUE')->whereBetween('reservasi_date',$between);
            }else{
                $sql = Antrian::whereBetween('reservasi_date',$between)->where('status','QUEUE');
            }
        }

        $antrian = $sql->latest()->paginate($perPage);
        $total = 0;
        $dine_in = 0;
        $reserved = 0;
        $user = User::count();
        $outlet = Outlet::count();


        return view('antrian.index', compact('antrian','from','to','keyword','total','dine_in','reserved','user','outlet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $outlets = Outlet::all();
        $logged = Auth()->user();

        if($logged->role != "Administrator"){
            $outlets = Outlet::where('id', $logged->outlet_id)->get();
        }

        return view('antrian.create', compact('outlets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $today = Carbon::now();
        $sequence = Antrian::where('outlet_id', $request->outlet_id)->whereDate('reservasi_date', $today->toDateString())->orderBy('no','desc')->first();
        $post = new Antrian;
        $post->no = $sequence ? ($sequence->no + 1) : 1;
        $post->created_by = auth()->user()->name;
        $post->updated_by = auth()->user()->name;
        $post->name = $request->name;
        $post->segmentasi = $request->segmentasi;
        $post->phone = $request->phone;
        $post->pax = $request->pax;
        $post->outlet_id = $request->outlet_id;
        $post->area = $request->area;
        $post->description = $request->description;
        $post->reservasi_date = Carbon::now();
        $post->save();


        return redirect('antrian')->with('flash_message', 'Post added!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function cancel($id)
    {
        $post = Antrian::find($id);
        $post->status = "CANCELED";
        $post->save();

        return redirect('antrian')->with('flash_message', 'CANCELED');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $antrian = Antrian::find($id);
        $antrian->status = "RESERVED";
        $antrian->save();

        $post = new Post;
        $post->created_by = auth()->user()->name;
        $post->updated_by = auth()->user()->name;
        $post->name = $antrian->name;
        $post->segmentasi = $antrian->segmentasi;
        $post->phone = $antrian->phone;
        $post->pax = $antrian->pax;
        $post->outlet_id = $antrian->outlet_id;
        $post->area = $antrian->area;
        $post->description = $antrian->description;
        $post->reservasi_date = $antrian->reservasi_date;
        $post->save();

        return redirect('antrian')->with('flash_message', 'RESERVED!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

}
