

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($user->name) ? $user->name : ''}}" required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email" value="{{ isset($user->email) ? $user->email : ''}}" required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Password' }}</label>
    <input class="form-control" name="password" type="password" id="password" value="{{ isset($user->password) ? $user->password : ''}}" required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('outlet') ? 'has-error' : ''}}">
    <label for="outlet" class="control-label">{{ 'Outlet' }}</label>
    <select class="form-control" name="outlet_id" id="outlet_id" required>
        <option value="">--PILIH--</option>
        @foreach ($outlets as $outlet)
            <option value="{{$outlet->id}}" {{ isset($user->outlet_id) && $outlet->id == $user->outlet_id ? "selected" : ''}}>{{$outlet->name}}</option>
        @endforeach
    </select>
    {!! $errors->first('outlet', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('outlet') ? 'has-error' : ''}}">
    <label for="outlet" class="control-label">{{ 'Role' }}</label>
    <select class="form-control" name="role"  id="role" required>
        <option value="">--PILIH--</option>
        <option value="Administrator" {{ isset($user->role) && $user->role == "Administrator" ? "selected" : ''}}>Administrator</option>
        <option value="Staff" {{ isset($user->role) && $user->role == "Staff" ? "selected" : ''}}>Staff</option>
    </select>
    {!! $errors->first('outlet', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group text-right">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
