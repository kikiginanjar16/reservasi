@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data History</div>
                <div class="card-body">
                    <div class="table-responsive" style="min-height: 450px">
                        <table class="table  table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 50%">Name Customer</th>
                                    <th style="width: 40%" >No HP</th>
                                    <th style="width: 10%">History</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><img src="{{asset('assets/images/user.png')}}" style="width:32px">  {{ $item->name }}</td>
                                        <td><img src="{{asset('assets/images/phone.png')}}" style="width:32px"> {{ $item->phone }}</td>
                                        <td>
                                            <a href="{{route('history.detail',['phone'=>$item->phone])}}" type="button" class="btn btn-info">History</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $posts->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
