@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="text-center" style="mt-1">
                    <br>
                @if($posts && count($posts))
                    <h2 style="mt-1"><img src="{{asset('assets/images/user.png')}}" style="width:52px">  <br>{{$posts[0]->name}}</h2>
                    <h3>({{$posts[0]->phone}})</h3>
                @endif
                <br><br>
                </div>
                <div class="card-header">Data History</div>
                <div class="card-body">
                    <a href="{{route('history.index')}}" class="btn btn-info">Back to History</a>
                    <br><br>
                    <div class="table-responsive" style="min-height: 450px">
                        <table class="table  table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width: 30%">Outlet</th>
                                    <th style="width: 50%">Tanggal & jam</th>
                                    <th style="width: 20%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><img src="{{asset('assets/images/outlet.png')}}" style="width:32px"> {{ $item->outlet->name }}</td>
                                        <td><img src="{{asset('assets/images/calender.png')}}" style="width:32px"> {{ $item->reservasi_date }}</td>
                                        <td><img src="{{asset('assets/images/dine.png')}}" style="width:32px"> {{ $item->status }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
