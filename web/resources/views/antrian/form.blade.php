{{-- <div class="form-group {{ $errors->has('no') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'No Antrian' }}</label>
    <input type="number" class="form-control" name="no" type="text" id="no" value="{{ isset($antrian->no) ? $antrian->no : ''}}" >
    {!! $errors->first('no', '<p class="help-block">:message</p>') !!}
</div> --}}
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Nama Customer' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($antrian->name) ? $antrian->name : ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'No HP' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($antrian->phone) ? $antrian->phone : ''}}" >
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pax') ? 'has-error' : ''}}">
    <label for="pax" class="control-label">{{ 'Pax' }}</label>
    <input class="form-control" name="pax" type="number" id="pax" value="{{ isset($antrian->pax) ? $antrian->pax : ''}}" >
    {!! $errors->first('pax', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('outlet') ? 'has-error' : ''}}">
    <label for="outlet" class="control-label">{{ 'Outlet' }}</label>
    <select class="form-control" name="outlet_id" type="number" id="outlet_id" value="{{ isset($antrian->outlet) ? $antrian->outlet : ''}}" >
        <option value="">--PILIH--</option>
        @foreach ($outlets as $outlet)
            <option value="{{$outlet->id}}">{{$outlet->name}}</option>
        @endforeach
    </select>
    {!! $errors->first('outlet', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('area') ? 'has-error' : ''}}">
    <label for="area" class="control-label">{{ 'Area' }}</label>
    <select name="area" class="form-control" id="area" >
    <option value="">--PILIH--</option>
    @foreach (json_decode('{"Smoking":"Smoking","No Smoking":"No Smoking"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($antrian->area) && $antrian->area == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('area', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('area') ? 'has-error' : ''}}">
    <label for="area" class="control-label">{{ 'Segmentasi' }}</label>
    <select name="segmentasi" class="form-control" id="segmentasi" >
    <option value="">--PILIH--</option>
    <option value="Family">Family</option>
    <option value="Corporate">Corporate</option>
    <option value="Government">Government</option>
    <option value="Millennials">Millennials</option>
    <option value="Professional">Professional</option>
    <option value="Education">Education</option>
    <option value="Community">Community</option>
</select>
    {!! $errors->first('area', '<p class="help-block">:message</p>') !!}
</div>
{{-- <div class="form-group {{ $errors->has('reservasi_date') ? 'has-error' : ''}}">
    <label for="reservasi_date" class="control-label">{{ 'Reservasi Date' }}</label>
    <input class="form-control datetimepicker" name="reservasi_date" type="text" id="reservasi_date" value="{{ isset($antrian->reservasi_date) ? $antrian->reservasi_date : ''}}" >
    {!! $errors->first('reservasi_date', '<p class="help-block">:message</p>') !!}
</div> --}}

<div class="form-group {{ $errors->has('pax') ? 'has-error' : ''}}">
    <label for="pax" class="control-label">{{ 'Keterangan' }}</label>
    <textarea class="form-control" name="description" id="description" rows="5">{{ isset($antrian->description) ? $antrian->description : ''}}</textarea>
    {!! $errors->first('pax', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group text-right">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
