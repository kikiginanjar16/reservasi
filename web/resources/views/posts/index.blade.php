@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Reservasi</div>
                <div class="card-body">


                    <form method="GET" action="{{ url('/posts') }}" accept-charset="UTF-8" role="search">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" class="datepicker" name="from" placeholder="From"
                                        style="width: 70%" value="{{ $from }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-info btn-sm text-muted" type="button">
                                            <i class="text-muted fa fa-calendar"
                                                style="color: #fff !important;font-size:auto"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" class="datepicker" name="to" placeholder="To" style="width: 70%"
                                        value="{{ $to }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-info btn-sm text-muted" type="button">
                                            <i class="text-muted fa fa-calendar"
                                                style="color: #fff !important;font-size:auto"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" name="search" placeholder="Search..." style="width: 70%"
                                        value="{{ $keyword }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-info btn-sm text-muted" type="button">
                                            <i class="text-muted i-Magnifi-Glass1"
                                                style="color: #fff !important;font-size:auto"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <a href="{{ url('/posts/create') }}" class="btn btn-success btn-sm" title="Add New Post">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                </a>
                                <button class="btn btn-info btn-sm text-muted" style="color: #fff !important;font-size:auto"
                                    type="submit">
                                    <i class="text-muted i-Magnifi-Glass1" style="color: #fff !important;font-size:auto">
                                    </i>
                                    Search
                                </button>
                                </a>
                            </div>
                    </form>


                    <br />
                    <br />
                    <br />
                    <div class="table-responsive" style="min-height: 450px">
                        <table class="table  table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal & Jam </th>
                                    <th>Segmentasi </th>
                                    <th class="text-center">Outlet</th>
                                    <th>Name Customer</th>
                                    <th class="text-center">No HP</th>
                                    <th class="text-center">Pax</th>
                                    <th class="text-center">Area</th>
                                    <th>Keterangan</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->reservasi_date }}</td>
                                        <td>{{ $item->segmentasi }}</td>
                                        <td class="text-center"><span
                                                class="badge badge-info">{{ isset($item->outlet->name) ? $item->outlet->name : 'Not Set' }}</span>
                                        </td>
                                        <td>{{ $item->name }}</td>
                                        <td class="text-center">{{ $item->phone }}</td>
                                        <td class="text-center">{{ $item->pax }}</td>
                                        <td class="text-center">
                                            @if ($item->area == 'No Smoking')
                                                <span class="badge badge-success">{{ $item->area }}</span>
                                            @else
                                                <span class="badge badge-warning">{{ $item->area }}</span>
                                            @endif
                                        </td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            {{-- <a href="{{ url('/posts/' . $item->id) }}" title="View Post"><button
                                                    class="btn btn-info btn-sm"><i class="fa fa-eye"
                                                        aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/posts/' . $item->id . '/edit') }}" title="Edit Post"><button
                                                    class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                        aria-hidden="true"></i> Edit</button></a> --}}

                                            <form method="POST" action="{{ url('/posts' . '/' . $item->id . '/cancel') }}"
                                                accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('PUT') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-primary btn-sm" title="Delete Post"
                                                    onclick="return confirm(&quot;Apakah anda yakin untuk status Cancel?&quot;)"> Cancel</button>
                                            </form>

                                            <form method="POST" action="{{ url('/posts' . '/' . $item->id) }}"
                                                accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Post"
                                                    onclick="return confirm(&quot;Apakah anda yakin untuk status Dine IN?&quot;)"> Dine In</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $posts->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
