@extends('layouts.app')

@section('content')
    <div class="container-x">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Data Outlet</div>
                    <div class="card-body">
                        <a href="{{ url('/outlet/create') }}" class="btn btn-success btn-sm" title="Add New Outlet">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/outlet') }}" accept-charset="UTF-8"
                            class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..."
                                    value="{{ request('search') }}">
                                <span class="input-group-append">
                                   <button class="btn btn-info btn-sm text-muted" type="submit">
                                    <i class="text-muted i-Magnifi-Glass1" style="color: #fff !important;font-size:16px"></i>
                                </button>
                                </span>
                            </div>
                        </form>

                        <br />
                        <br />
                        <div class="table-responsive"  style="min-height: 450px">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">#</th>
                                        <th style="width: 25%">Name</th>
                                        <th style="width: 25%">Description</th>
                                        <th style="width: 45%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($outlet as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>
                                                <a href="{{ url('/outlet/' . $item->id) }}" title="View Outlet"><button
                                                        class="btn btn-info btn-sm"><i class="fa fa-eye"
                                                            aria-hidden="true"></i> View</button></a>
                                                <a href="{{ url('/outlet/' . $item->id . '/edit') }}"
                                                    title="Edit Outlet"><button class="btn btn-primary btn-sm"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit</button></a>

                                                <form method="POST" action="{{ url('/outlet' . '/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Delete Outlet"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $outlet->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
