<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/history', 'HistoryController@index')->name('history.index');
Route::get('/history/{phone}', 'HistoryController@detail')->name('history.detail');
Route::resource('posts', 'PostsController');
Route::put('/posts/{id}/cancel', 'PostsController@cancel')->name('posts.cancel');

Route::resource('outlet', 'OutletController');
Route::resource('user', 'UserController');
Route::resource('antrian', 'AntrianController');

Route::put('/antrian/{id}/cancel', 'AntrianController@cancel')->name('antrian.cancel');

